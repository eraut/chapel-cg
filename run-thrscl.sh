#!/bin/bash

set -o xtrace

chpl --fast cg.chpl

for nthr in 1 2 4 8; do
    export CHPL_RT_NUM_THREADS_PER_LOCALE=$nthr
    numactl -m 0 -N 0 ./cg
done
