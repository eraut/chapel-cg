use Random;
use Time;

config const N = 4;
config const B = 2;


const vD = {1..N};
const mD = {1..N, 1..N};
//------------------------------------------------

proc dotprod(a: [vD] real, b: [vD] real)
{
  var sum: real = 0.0;
  for i in 1..N {
    sum += a[i] * b[i];
  }
  return sum;
}

proc mtxvecprod(M: [mD] real, v: [vD] real, ref p: [vD] real)
{
  const NpB = N / B;
  coforall bid in 1..B {
    const rs = (bid-1)*NpB+1;
    for row in rs..(rs+NpB-1) {
      p[row] = 0.0;
      for col in 1..N {
        p[row] += M[row,col]*v[col];
      }
    }
  }
}

//------------------------------------------------
var randStream = new owned RandomStream(real,0,false);

var A: [mD] real;
var b: [vD] real;

writeln("Init A");
randStream.fillRandom(A);
for r in 1..N {
  for c in r..N {
    if r == c {
      A[r,c] += sqrt(N);
    }
    else {
      A[r,c] = 0.5*(A[r,c] + A[c,r]);
      A[c,r] = A[r,c];
    }
  }
}

writeln("Init b");
randStream.fillRandom(b);

// Init x = 0
var x: [vD] real = 0.0;

var r: [vD] real = b;
var p: [vD] real = r;

var Ap: [vD] real;

config param tol = 1.0e-12;
config param maxiter = 50;
var k = 1;

writeln("Begin iterations");
const start = getCurrentTime(TimeUnits.seconds);
var rTr: real = dotprod(r,r);
while true
{
  mtxvecprod(A, p, Ap);
  var pTAp: real = dotprod(p, Ap);
  var alpha = rTr / pTAp;

  x += alpha * p;
  r -= alpha * Ap;

  var rTr_new: real = dotprod(r,r);
  writeln("residual = ", rTr_new);

  if (rTr_new < tol) {
    writeln("Converged after ", k, " iterations!");
    break;
  } else if (k > maxiter) {
    writeln("Failed to converge after ", k, " iterations!");
    break;
  }

  const beta: real = rTr_new / rTr;

  p *= beta;
  p += r;

  rTr = rTr_new;
  k += 1;
}
const end = getCurrentTime(TimeUnits.seconds);
writeln("Time: ",end-start);
